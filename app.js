var calc = require("./js/calculator");
const express = require("express");
const app = express();
const ejs = require("ejs");
const bodyParser = require('body-parser');
const mysql = require("mysql");
const Joi = require("joi");

class Database {
    constructor( config ) {
        this.connection = mysql.createConnection(config);
    }
    query( sql, args ) {
        return new Promise( (resolve, reject) => {
            this.connection.query(sql, args, (err, rows) => {
                if (err) return reject(err);
                resolve(rows);
            });
        });
    }
    close() {
        return new Promise( (resolve, reject) => {
            this.connection.end( (err) => {
                if (err) return reject(err);
                resolve();
            });
        });
    }
}

const db_config = {
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'rolety',
};

var database = new Database(db_config);

var jsonParser = bodyParser.json();

app.set('view engine', 'ejs');

app.use('/js', express.static('js'));

app.get('/', (req, res) => {
    res.render("index");
});

app.get('/rolety/:table', (req, res) => {
    var table = req.params.table;
    if (table === 'typy') {
        database.query("SELECT * FROM typy")
        .then( rows => {
            res.json(JSON.stringify(rows));
        })
    }
    if (table === 'rastry') {
        database.query("SELECT * FROM rastry")
        .then( rows => {
            res.json(JSON.stringify(rows));
        })
    }
    if (table === 'kolorystyka') {
        database.query("SELECT * FROM kolorystyka")
        .then( rows => {
            res.json(JSON.stringify(rows));
        })
    }
    if (table === 'listwy_montazowe') {
        database.query("SELECT * FROM listwy_montazowe")
        .then( rows => {
            res.json(JSON.stringify(rows));
        })
    }

});

const validatorSchema = Joi.object().keys({
    typy_id: Joi.number().integer().min(1).required(),
    kolorystyka_id: Joi.number().integer().min(1).required(),
    listwy_montazowe_id: Joi.number().integer().min(1).required(),
    miara: Joi.string().min(2).max(3).required(),
    wysokosc: Joi.number().integer().min(1).required(),
    szerokosc: Joi.number().integer().min(1).required(),
});


app.post('/validate_order', jsonParser, (req, res) => {
    const {error, value} = Joi.validate(req.body, validatorSchema);
    if (error != null) {
        console.log(err);
        res.sendStatus(400);
    }
    let typ_id;
    let raster;
    let procentowy_koszt;
    let listwa;

    database.query("SELECT id FROM typy WHERE id = ? LIMIT 1", [value.typy_id])
    .then( rows => {
        console.log(rows[0].id);
        typ_id = rows;
        return database.query("SELECT * FROM rastry WHERE id_typu = ? AND szer_od <= ? AND szer_do > ? AND wys_od <= ? AND szer_do > ? LIMIT 1",
            [value.typy_id, value.szerokosc, value.szerokosc, value.wysokosc, value.wysokosc]);
    })
    .then( rows => {
        raster = rows;
        if(typ_id[0].id !== rows[0].id_typu) {
            res.sendStatus(400);
        };
        return database.query("SELECT procentowy_koszt FROM kolorystyka WHERE id = ? LIMIT 1", [value.kolorystyka_id]);
    })
    .then( rows => {
        procentowy_koszt = rows;
        return database.query("SELECT cena, miara FROM listwy_montazowe WHERE id = ? LIMIT 1", [value.listwy_montazowe_id]);
    })
    .then( rows => {
        listwa = rows;
        let result = calc.oblicz(
            raster[0].cena, value.wysokosc, procentowy_koszt[0].procentowy_koszt, listwa[0].cena, listwa[0].miara
        );
        res.status(200).json({
            result: result.toFixed(2)
        });
    })
    .catch( err => {
        console.log(err);
    })
});

app.listen(8000, () => console.log("Listening on port 8000"));