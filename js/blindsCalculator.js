angular.
    module("blindsCalculator", [])
    .controller("formController", ['dbService', 'pricingCalculator', 'Validator', formController])
    .factory('dbService', ['$http', dbService])
    .factory('Validator', ['$http', Validator])
    .factory('pricingCalculator', pricingCalculator);

function formController(dbService, pricingCalculator, Validator) {
    var vm = this;
    vm.typy = [];
    vm.rastry = [];
    vm.kolorystyka = [];
    vm.listwyMontazowe = [];
    vm.cena = 0;
    vm.selectedTyp = {};
    vm.selectedKolor = {};
    vm.selectedListwaMontazowa = {};
    vm.szerokosc = 0;
    vm.wysokosc = 0;
    getTypy();
    getRastry();
    getKolorystyka();
    getListwyMontazowe();

    function getRastry() {
        dbService.getRastry().then( data => {
            vm.rastry = angular.fromJson(data);
        })
        .catch( err => {console.log(err)});
    };
    function getListwyMontazowe() {
        dbService.getListwyMontazowe().then( data => {
            vm.listwyMontazowe = angular.fromJson(data);
        })
        .catch( err => {console.log(err)});
    };
    function getKolorystyka() {
        dbService.getKolorystyka().then( data => {
            vm.kolorystyka = angular.fromJson(data);
        })
        .catch( err => {console.log(err)});
    };
    function getTypy() {
        dbService.getTypy().then( data => {
            vm.typy = angular.fromJson(data);
        })
        .catch( err => {console.log(err)});
    };
    vm.submitForm = function (formValid) {

        if(Validator.isFormValid(
            vm.selectedTyp.id,
            vm.selectedKolor.id,
            vm.selectedListwaMontazowa.id,
            vm.rastry,
            vm.wysokosc,
            vm.szerokosc,
        )) {
            Validator.validateOrder(
                vm.selectedTyp.id,
                vm.selectedKolor.id,
                vm.selectedListwaMontazowa.id,
                vm.selectedListwaMontazowa.miara,
                vm.wysokosc,
                vm.szerokosc,
            ).then( res => {
                vm.message = "Twoja cena(sprawdzona przez serwer):" + res.data.result + " PLN";
            });
        }
        else {
            vm.message = "Prosze o wypełnienie formularza.";
        }

    };
    vm.checkPrice = function () {
        if (Validator.isFormValid(
            vm.selectedTyp.id,
            vm.selectedKolor.id,
            vm.selectedListwaMontazowa.id,
            vm.rastry,
            vm.wysokosc,
            vm.szerokosc,
        )) {
            vm.cena = pricingCalculator.calculate(
                vm.selectedTyp, vm.rastry,
                vm.szerokosc, vm.wysokosc,
                vm.selectedKolor, vm.selectedListwaMontazowa
            );
        }
        else {
            vm.message = "Prosze o wypelnienie formularza.";
        }
    };
};

function dbService($http) {
    function getTypy() {
        return $http.get("/rolety/typy").then( response => {
            return response.data;
        });
    }
    function getRastry() {
        return $http.get("/rolety/rastry").then( response => {
            return response.data;
        });
    }
    function getListwyMontazowe() {
        return $http.get("/rolety/listwy_montazowe").then( response => {
            return response.data;
        });
    }
    function getKolorystyka() {
        return $http.get("/rolety/kolorystyka").then( response => {
            return response.data;
        });
    }
    return {
        'getTypy': getTypy,
        'getRastry': getRastry,
        'getListwyMontazowe': getListwyMontazowe,
        'getKolorystyka': getKolorystyka,
    }
};

function Validator($http) {
    return {
        validateOrder: function(typy_id, kolorystyka_id, listwy_montazowe_id,
                                miara, wysokosc, szerokosc) {
            return $http.post("/validate_order", JSON.stringify({
                typy_id,
                kolorystyka_id,
                listwy_montazowe_id,
                miara,
                wysokosc,
                szerokosc,
            }));
        },
        isFormValid: function(typy_id = 0, kolorystyka_id = 0, listwy_montazowe_id = 0, rastry, wys, szer) {
            if (typy_id === 0 || kolorystyka_id === 0 ||
                listwy_montazowe_id === 0) {
                    return false;
                }
            var rastry_typu = rastry.filter( elem => {
                return elem.id_typu === typy_id;
            });
            let min_szer = Math.min(...rastry_typu.map( elem => {
                return elem.szer_od;
            }));
            let max_szer = Math.max(...rastry_typu.map( elem => {
                return elem.szer_do;
            }));
            let min_wys = Math.min(...rastry_typu.map( elem => {
                return elem.wys_od;
            }));
            let max_wys = Math.max(...rastry_typu.map( elem => {
                return elem.wys_do;
            }));

            if (szer >= min_szer && szer < max_szer &&
                wys >= min_wys && wys < max_wys) {
                return true;
            } else {
                return false;
            }
        },
    }
};

function pricingCalculator() {
    return {
        calculate: function(typ, rastry, szerokosc, wysokosc, kolor, listwa_montazowa) {
            let rastry_typu = rastry.filter( elem => {
                return elem.id_typu === typ.id &&
                       szerokosc >= elem.szer_od && szerokosc < elem.szer_do &&
                       elem.wys_od <= wysokosc && elem.wys_do > wysokosc;
            });
            if (typeof rastry_typu[0] == undefined) return 0;
            return calculator.oblicz(rastry_typu[0].cena, wysokosc, kolor.procentowy_koszt, listwa_montazowa.cena, listwa_montazowa.miara);
        },
    }
}