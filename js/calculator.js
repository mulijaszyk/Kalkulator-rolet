(function(exports) {
    exports.oblicz = function(cenaRastra, wysokoscRastra, kosztKoloru, cenaListwy, miaraListwy){
        var result = cenaRastra*(1+kosztKoloru);
        if(miaraListwy === "szt") {
            result += cenaListwy;
        }
        else if(miaraListwy === "mb") {
            result += cenaListwy * (wysokoscRastra / 1000);
        }
        else {
            throw TypeError("Invalid argument miaraListwy");
        }
        return result;
    };
})(typeof exports === 'undefined' ? this['calculator'] = {} : exports);