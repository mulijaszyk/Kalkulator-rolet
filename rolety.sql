-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: rolety
-- ------------------------------------------------------
-- Server version	5.5.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `kolorystyka`
--

DROP TABLE IF EXISTS `kolorystyka`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kolorystyka` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `procentowy_koszt` decimal(3,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kolorystyka`
--

LOCK TABLES `kolorystyka` WRITE;
/*!40000 ALTER TABLE `kolorystyka` DISABLE KEYS */;
INSERT INTO `kolorystyka` VALUES (1,'Ciemiezny Czerwony',0.40),(2,'Farszowany Szary',0.10),(3,'Intubowany Zolty',0.12),(4,'Pokrecony Pomarancz',0.21);
/*!40000 ALTER TABLE `kolorystyka` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `listwy_montazowe`
--

DROP TABLE IF EXISTS `listwy_montazowe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `listwy_montazowe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cena` decimal(10,2) DEFAULT NULL,
  `miara` enum('mb','szt') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `listwy_montazowe`
--

LOCK TABLES `listwy_montazowe` WRITE;
/*!40000 ALTER TABLE `listwy_montazowe` DISABLE KEYS */;
INSERT INTO `listwy_montazowe` VALUES (1,'Aluminiowa',25.00,'szt'),(2,'Plastikowa PCV',12.50,'szt'),(3,'Karbonowa na wymiar',75.29,'mb');
/*!40000 ALTER TABLE `listwy_montazowe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rastry`
--

DROP TABLE IF EXISTS `rastry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rastry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_typu` int(11) DEFAULT NULL,
  `cena` decimal(10,2) DEFAULT NULL,
  `szer_od` int(11) DEFAULT NULL,
  `szer_do` int(11) DEFAULT NULL,
  `wys_od` int(11) DEFAULT NULL,
  `wys_do` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_typu` (`id_typu`),
  CONSTRAINT `rastry_ibfk_1` FOREIGN KEY (`id_typu`) REFERENCES `typy` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rastry`
--

LOCK TABLES `rastry` WRITE;
/*!40000 ALTER TABLE `rastry` DISABLE KEYS */;
INSERT INTO `rastry` VALUES (1,1,249.99,250,750,250,750),(2,1,300.00,750,1250,750,1250),(3,1,320.00,1250,1750,1250,1750),(4,2,150.00,100,600,100,600),(5,2,190.00,100,600,600,1200),(6,2,190.00,100,600,600,1200);
/*!40000 ALTER TABLE `rastry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `typy`
--

DROP TABLE IF EXISTS `typy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typy`
--

LOCK TABLES `typy` WRITE;
/*!40000 ALTER TABLE `typy` DISABLE KEYS */;
INSERT INTO `typy` VALUES (1,'Elektryczna'),(2,'Ręczna');
/*!40000 ALTER TABLE `typy` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-06 10:08:55
